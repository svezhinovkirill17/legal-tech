import React from 'react'
import { Button } from "@blueprintjs/core";
import styles from "./header.css";
const Header = () => {
  return (
    <div className={styles.container}>
      <div className={styles.headerTitle}>Тестовое <br/> задание</div>
      <Button>
        Войти
      </Button>
    </div>
  )
}

export default Header