import React, { useState } from "react";
import { Button, Dialog, InputGroup } from "@blueprintjs/core";
import styles from "./loginForm.module.css";

const LoginForm = () => {
  const [login, setLogin] = useState("");
  return (
    <Dialog isOpen={true} autoFocus className={styles.login}>
      <div className={styles.loginTitle}>Войти</div>
      <InputGroup value={login} onInput={(e) => setLogin(e.target.value)} large title="Логин" placeholder="Логин" className={styles.loginInput} />
      <InputGroup
        value={login}
        onInput={(e) => setLogin(e.target.value)}
        type="password"
        large
        leftIcon="lock"
        title="Пароль"
        placeholder="пароль"
        className={styles.loginInput}
      />
      <Button className={styles.loginBtn}>Войти</Button>
    </Dialog>
  );
};

export default LoginForm;
